import json
import datetime
import sys
from pycoingecko import CoinGeckoAPI


cg = CoinGeckoAPI()
coin_ticker = {'btc': 'bitcoin', 'eth': 'ethereum', 'ltc': 'litecoin', 'xmr': 'monero', 'xrp': 'ripple'}
# coin_ticker = {name["symbol"]: name["id"] for name in cg.get_coins_list()}
print(json.dumps({"Rate": cg.get_price(ids=coin_ticker[sys.argv[-3].lower()], vs_currencies=sys.argv[-2])[coin_ticker[sys.argv[-3].lower()]][sys.argv[-2].lower()], "Time": datetime.datetime.utcnow().isoformat()}))


'''
import json
import datetime
import sys
from pycoingecko import CoinGeckoAPI

id_a = sys.argv[-3]
id_b = sys.argv[-2]
api_key = sys.argv[-1]

cg = CoinGeckoAPI()
coins = cg.get_coins_list()
coin_ticker = {'btc': 'bitcoin', 'eth': 'ethereum', 'ltc': 'litecoin', 'xmr': 'monero', 'xrp': 'ripple'}
# coin_ticker = {}
# for name in coins:
#     coin_ticker[name["symbol"]] = name["id"]

id_a_name = coin_ticker[id_a.lower()]

rate = cg.get_price(ids=id_a_name, vs_currencies=id_b)
print(json.dumps({"IdA": id_a, "IdB": id_b, "Rate": rate[id_a_name][id_b.lower()], "Time": datetime.datetime.utcnow().isoformat()}))
'''