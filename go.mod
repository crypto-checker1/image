module gitlab.com/colourdelete/image

go 1.15

require (
	github.com/golang/freetype v0.0.0-20170609003504-e2365dfdc4a0 // indirect
	github.com/pelletier/go-toml v1.8.1
	github.com/srwiley/oksvg v0.0.0-20200311192757-870daf9aa564
	github.com/srwiley/rasterx v0.0.0-20200120212402-85cb7272f5e9
	golang.org/x/image v0.0.0-20200927104501-e162460cd6b5 // indirect
	golang.org/x/net v0.0.0-20201110031124-69a78807bb2b // indirect
	gopkg.in/fogleman/gg.v1 v1.3.0
)
