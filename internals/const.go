package internals

import "errors"

var programName = "Crypto Checker Image Renderer"                           //nolint:gochecknoglobals
var programVersion = "0.1.0-alpha"                                          //nolint:gochecknoglobals
var programAuthor = "Ken Shibata (@colourdelete on GitLab.com, GitHub.com)" //nolint:gochecknoglobals

var renderCheckError = errors.New("config malformed")
var unsupportedFormatError = errors.New("unsupported export format - this is a bug, please report it at <https://gitlab.com/crypto-checker1/image>")
