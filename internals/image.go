package internals

import (
	"gitlab.com/colourdelete/image/pkg"

	"fmt"
	"gopkg.in/fogleman/gg.v1"
	"image"
	"log"
	"os"
	"strconv"
	"time"
)

func CleanIcon(cfg *RenderConfig, rate Rate) (image.Image, error, bool) {
	var err error

	var icon image.Image
	var iconOk = false
	var iconPath = fmt.Sprintf(cfg.Icon.PathFormat, rate.ID)

	switch cfg.Icon.Format {
	case "png":
		icon, err = gg.LoadImage(iconPath)
		if err != nil {
			log.Printf("[%s icon] opening file as image %s failed", rate.ID, iconPath)
		}
		iconOk = true
	case "svg":
		var rawIcon, iconFile *os.File
		var iconFilePath = fmt.Sprintf("%s.png", iconPath)
		var iconSize = [2]int{
			int(float64(cfg.Icon.Size[0]) * rate.Size),
			int(float64(cfg.Icon.Size[1]) * rate.Size),
		}
		rawIcon, err = os.Open(iconPath)

		if err != nil {
			log.Printf("[%s icon] opening file %s failed: %v", rate.ID, iconPath, err)
			break // silently fail icon
		}

		iconFile, err = os.Create(iconFilePath)
		if err != nil {
			log.Printf("[%s icon] creating file %s failed: %v", rate.ID, iconPath, err)
			break // silently fail icon
		}

		err = pkg.ConvSVGToPNG(rawIcon, iconFile, iconSize[0], iconSize[1])
		if err != nil {
			log.Printf("[%s icon] converting file %s to png failed", rate.ID, iconPath)
			break // silently fail icon
		}

		if err = rawIcon.Close(); err != nil {
			log.Panicf("[%s icon] closing file failed: %v", rate.ID, err)
		}

		if err = iconFile.Close(); err != nil {
			log.Panicf("[%s icon] closing file failed: %v", rate.ID, err)
		}

		icon, err = gg.LoadImage(iconFilePath)
		if err != nil {
			log.Printf("[%s icon] opening file as image %s failed: %v", rate.ID, iconPath, err)
			break // silently fail icon
		}
		iconOk = true
	}
	return icon, err, iconOk
}

func RenderImage(rates []Rate, cfg *RenderConfig) error {
	var err error

	ctx := gg.NewContext(cfg.Width, cfg.Height)
	ctx.SetRGB(0, 0, 0)
	ctx.Clear()
	ctx.SetRGB(1, 1, 1)
	width := 1.0
	lineSpacing := 1.0
	align := gg.AlignCenter

	for _, rate := range rates {

		log.Printf("[%s icon] cleaning icon", rate.ID)
		icon, err, iconOk := CleanIcon(cfg, rate)

		if err == nil && iconOk { // protect against nil pointer dereference
			log.Printf("[%s icon] drawing icon", rate.ID)
			ctx.DrawImageAnchored(
				icon,
				int(rate.Pos[0]+cfg.Icon.Offset[0]*rate.Size*float64(cfg.Icon.Size[0])),
				int(rate.Pos[1]+cfg.Icon.Offset[1]*rate.Size*float64(cfg.Icon.Size[1])),
				0.5,
				0.5,
			)
		} else {
			log.Printf("[%s icon] no icon", rate.ID)
		}

		log.Printf("[%s text] loading font", rate.ID)
		if err := ctx.LoadFontFace(cfg.Font.Path, float64(cfg.Font.Size)*rate.Size); err != nil {
			return err
		}

		var fmtedPrice = ""
		if rate.Price != 0.0 {
			fmtedPrice = fmt.Sprintf(rate.Format, strconv.FormatFloat(rate.Price, 'f', -1, 64))
		}
		log.Printf("[%s text] drawing string", rate.ID)
		ctx.DrawStringWrapped(
			fmtedPrice,
			rate.Pos[0],
			rate.Pos[1],
			0.5,
			0.5,
			width,
			lineSpacing,
			align,
		)
	}

	log.Print("[timestamp] loading font & generating timestamp")
	if err := ctx.LoadFontFace(cfg.Font.Path, cfg.Timestamp.FontSize); err != nil {
		return err
	}
	var timestamp = fmt.Sprintf(cfg.Timestamp.Format, time.Now().Format(time.RFC3339))
	log.Print("[timestamp] drawing timestamp")

	ctx.DrawStringAnchored(
		timestamp,
		cfg.Timestamp.Pos[0],
		cfg.Timestamp.Pos[1],
		0.5,
		0.5,
	)

	switch cfg.Format {
	case "png":
		err = ctx.SavePNG(cfg.Path)
		if err != nil {
			log.Printf("[save] saving file %s failed", cfg.Path)
			log.Panic(err)
		}
		log.Printf("[save] saving file %s ok", cfg.Path)
	default:
		log.Panic(unsupportedFormatError)
	}

	return err
}
