package internals

import (
	"encoding/json"
	"fmt"
	"log"
	"os/exec"
)

type RawRate struct {
	Rate float64
	Time string
}

func getRawRate(cfg *Rates, idA, idB string) (*RawRate, error) {
	if len(cfg.SetupCmd) > 0 {
		cmd := exec.Command(cfg.SetupCmd[0], cfg.SetupCmd[1:]...)
		out, err := cmd.CombinedOutput()
		fmt.Println(string(out))
		if err != nil {
			return nil, err
		}
	}
	cmd := exec.Command(cfg.InterpPath, cfg.ScriptPath, idA, idB, cfg.ApiKey)
	out, err := cmd.CombinedOutput()
	fmt.Println(string(out))
	if err != nil {
		return nil, err
	}

	rawRate := &RawRate{}

	err = json.Unmarshal(out, rawRate)
	if err != nil {
		return nil, err
	}

	if rawRate == nil {
		return nil, fmt.Errorf("rawrate is blank: %v", rawRate)
	}

	return rawRate, nil
}

func goGetRawRate(cfg *Rates, idA, idB string, rawRateChan chan RawRate, doneChan chan error) {
	var err error
	rawRate, err := getRawRate(cfg, idA, idB)
	if err != nil {
		log.Panic(err)
	}
	rawRateChan <- *rawRate
	doneChan <- err
}
