package internals

import (
	"flag"
	"fmt"
	"log"
)

func getRates(cfg *Config, rates []Rate) ([]Rate, []error) {
	newRates := make([]Rate, len(rates))
	errs := make([]error, len(rates))
	rawRateChans := make([]chan RawRate, len(rates))
	doneChans := make([]chan error, len(rates))
	for i, rate := range rates {
		if rate.ID == "logo" {
			newRates[i] = rates[i]
			continue
		}
		rawRateChans[i] = make(chan RawRate)
		doneChans[i] = make(chan error)
		go goGetRawRate(cfg.Render.Rates, rate.ID, cfg.Render.Rates.IdB, rawRateChans[i], doneChans[i])
	}
	for i, rate := range rates {
		if rate.ID == "logo" {
			newRates[i] = rates[i]
			continue
		}
		rawRate := <-rawRateChans[i]
		err := <-doneChans[i]
		errs[i] = err
		newRates[i] = rates[i]
		if err == nil {
			newRates[i].Price = rawRate.Rate
		}
		// TODO: Add timestamp under each rate using rawRate.Time
	}
	return newRates, errs
}

func Run() {
	log.Printf("%s v%s by %s", programName, programVersion, programAuthor)
	log.Printf("Note: Licensing will be sorted out in the future.")
	var configPath = flag.String("config", "./config.toml", "config file path")
	//var contents []Content
	var cfg *Config
	var err error
	flag.Parse()
	if *configPath == "" {
		log.Fatal("config path must not be blank")
	} else {
		cfg, err = loadConfigFromPath(*configPath)
		if err != nil {
			log.Println("config load from path failed")
			log.Fatal(err)
		}
	}
	newRates, errs := getRates(cfg, cfg.Rates)
	fmt.Println(newRates)
	log.Print(errs)
	err = RenderImage(newRates, &cfg.Render)
	if err != nil {
		log.Fatal(err)
	}
}
