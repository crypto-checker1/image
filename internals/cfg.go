package internals

import (
	"github.com/pelletier/go-toml"
	"io/ioutil"
	"log"
)

const allowedRenderFormat = "png" // Only `png` for now TODO: Add other formats

type Config struct {
	Render RenderConfig
	Rates  []Rate
}

type RenderConfig struct {
	Timestamp *Timestamp
	Icon      *Icon
	Font      *Font
	Rates     *Rates
	Width     int
	Height    int
	Path      string
	Format    string
}

type Rates struct {
	InterpPath string
	SetupCmd   []string
	ScriptPath string
	ApiKey     string
	IdB        string
}

type Font struct {
	Path string
	Size int
}

type Icon struct {
	PathFormat string
	Format     string
	Size       [2]int
	Offset     [2]float64
}

type Timestamp struct {
	FontSize float64
	Pos      [2]float64
	Format   string
}

type Rate struct {
	ID     string
	Price  float64
	Pos    [2]float64
	Size   float64
	Format string
}

func (c *Config) Check() error {
	var allowedIconFormats = [2]string{"png", "svg"}
	var renderCheck, ratesCheck, renderIconCheck, renderTimestampCheck, renderFontCheck, renderRatesCheck bool
	ratesCheck = true

	renderCheck = c.Render.Width != 0 &&
		c.Render.Height != 0 &&
		c.Render.Path != "" &&
		c.Render.Format == allowedRenderFormat

	if !renderCheck {
		log.Print("Render check failed")
	}

	if c.Render.Timestamp == nil {
		log.Fatal("Render timestamp is nil")
	}

	renderTimestampCheck = c.Render.Timestamp.FontSize != 0.0 &&
		c.Render.Timestamp.Format != ""

	if !renderTimestampCheck {
		log.Print("Render timestamp check failed")
	}

	if c.Render.Rates == nil {
		log.Fatal("Render rates is nil")
	}

	renderRatesCheck = c.Render.Rates.InterpPath != "" &&
		c.Render.Rates.ScriptPath != "" &&
		c.Render.Rates.ApiKey != "" &&
		c.Render.Rates.IdB != ""

	if !renderRatesCheck {
		log.Print("Render rates check failed")
	}

	if c.Render.Font == nil {
		log.Fatal("Render font is nil")
	}

	renderFontCheck = c.Render.Font.Size != 0.0 &&
		c.Render.Font.Path != ""

	if !renderFontCheck {
		log.Print("Render font check failed")
	}

	if c.Render.Icon == nil {
		log.Fatal("Render icon is nil")
	}

	renderIconCheck = c.Render.Icon.Format != "" &&
		c.Render.Icon.Format != allowedIconFormats[0] ||
		c.Render.Icon.Format != allowedIconFormats[1] &&
			c.Render.Icon.Size != [2]int{0, 0} &&
			c.Render.Icon.PathFormat != ""

	if !renderIconCheck {
		log.Print("Render icon check failed")
	}

	if c.Rates == nil {
		log.Fatal("Rates is nil")
	}

	for i, rate := range c.Rates {
		rateCheck := rate.Size != 0.0 &&
			rate.Pos != [2]float64{0.0, 0.0} &&
			rate.ID != "" &&
			rate.Format != ""
		if !rateCheck {
			log.Printf("Rate #%v check failed", i)
		}
		ratesCheck = ratesCheck && rateCheck
	}

	if renderCheck && renderIconCheck && renderTimestampCheck && renderFontCheck && renderRatesCheck && ratesCheck {
		return nil
	}

	return renderCheckError
}

func loadConfigFromPath(path string) (*Config, error) {
	var err error
	var src []byte
	var cfg = &Config{}

	src, err = ioutil.ReadFile(path)

	if err != nil {
		return nil, err
	}

	err = toml.Unmarshal(src, cfg)
	if err != nil {
		return nil, err
	}

	err = cfg.Check()
	if err != nil {
		return nil, err
	}

	return cfg, nil
}
