package main

import (
	"gitlab.com/colourdelete/image/internals"
)

func main() {
	internals.Run()
}
