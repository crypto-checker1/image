package pkg

import (
	"github.com/srwiley/oksvg"
	"github.com/srwiley/rasterx"
	"image"
	"image/png"
	"os"
)

func ConvSVGToPNG(In, Out *os.File, w, h int) error {
	icon, _ := oksvg.ReadIconStream(In)
	icon.SetTarget(0, 0, float64(w), float64(h))
	rgba := image.NewRGBA(image.Rect(0, 0, w, h))
	icon.Draw(rasterx.NewDasher(w, h, rasterx.NewScannerGV(w, h, rgba, rgba.Bounds())), 1)
	err := png.Encode(Out, rgba)
	return err
}
